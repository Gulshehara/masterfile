package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
		String str = "Gulshehara";
		 
        // convert String to character array
        // by using toCharArray
        char[] ch = str.toCharArray();
 
        for (int i = ch.length - 1; i >= 0; i--)
        {
            System.out.print(ch[i]);
        }
	}

}
